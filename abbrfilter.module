<?php
/**
 *
 *
 *
 *
 *  ABBR Filter
 *  Eric Davila - UCSF School of Pharmacy
 *
 *  Adapted from Wordfilter and Pirate modules
 *
 * If you wish to attempt to filter other text in your theme,
 * call the function abbrfilter_filter_process($text)
 *
 * @file
 * Replaces abbrs inside posts with filtered versions.
 */

/**
 * Implementation of hook_help().
 *
 * @param $section
 *   string file path
 *
 * @return
 *   string
 */
function abbrfilter_help($page = 'admin/help#abbrfilter', $arg) {
  switch ($page) {
    case 'admin/modules#description':
      return t('Replaces abbreviations inside posts with ABBR tags.');
    case 'admin/help#abbrfilter':
      return t('The ABBRfilter module allows you to filter abbreviations in site content and replace the filtered abbreviations with ABBR tags and a title descriptor that contains the unabbreviated phrase. The text body of node and comments are filtered. The filters are applied on content viewing so the original text of your content is not altered.');
    case 'admin/settings/abbrfilter':
      return t('In order for filtering to work on the body text of a node or comment, you must activate the ABBRfilter filter for the input formats you wish to enable filtering for. Check your filter settings at <a href="@filter">Input Formats</a>.', array('@filter' => url('admin/settings/filters')));
  }
}

/**
 * Implementation of hook_perm().
 *
 * @return
 *   array of permissions
 */
function abbrfilter_perm() {
  return array('administer abbreviations filtered');
}

/**
 * Implementation of hook_menu().
 *
 * @param $may_cache
 *   boolean indicating whether cacheable menu items should be returned
 *
 * @return
 *   array of menu information
 */
function abbrfilter_menu() {
  $items = array();

  $items['admin/settings/abbrfilter'] = array(
    'title' => 'ABBR filter',
    'description' => 'Replaces abbreviations inside posts with ABBR tags.',
    'page callback' => 'abbrfilter_admin_list',
    'access arguments' => array('administer abbreviations filtered'),
  );

  $items['admin/settings/abbrfilter/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'access arguments' => array('administer abbreviations filtered'),
    'weight' => -10,
  );

  $items['admin/settings/abbrfilter/add'] = array(
    'title' => 'Add',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('abbrfilter_admin_add_form'),
    'access arguments' => array('administer abbreviations filtered'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/abbrfilter/test'] = array(
    'title' => 'Test',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('abbrfilter_admin_test_filter_form'),
    'access arguments' => array('administer abbreviations filtered'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/abbrfilter/edit/%'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('abbrfilter_admin_edit_form', 4),
    'access arguments' => array('administer abbreviations filtered'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/abbrfilter/delete/%'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('abbrfilter_admin_form_delete_confirm', 4),
    'access arguments' => array('administer abbreviations filtered'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Delete a abbr from the abbr filter list delete
 * confirmation callback
 *
 * @param $abbr_id
 *   The ID of the abbreviation to delete
 *
 * @return
 *  returns a confirmation (are you sure) form
 */
function abbrfilter_admin_form_delete_confirm($form, $abbr_id) {
  $form = array();
  $form['abbr_id'] = array(
    '#type' => 'value',
    '#value' => $abbr_id,
  );
  return confirm_form($form, t('Are you sure you want to delete this abbreviation from the ABBR filtering list?'), 'admin/settings/abbrfilter');
}

/**
 * Delete a abbr from the abbr filter list
 */
function abbrfilter_admin_form_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query('DELETE FROM {abbrfilter} WHERE id = %d', $form_state['values']['abbr_id']);
    drupal_set_message(t('The abbreviation was removed from the ABBR filter list'));
    $form_state['redirect'] = 'admin/settings/abbrfilter';
    cache_clear_all('*', 'cache_filter', TRUE);
  }
}

/**
 * Implementation of hook_block().
 *
 * @param $op
 *   string for view type
 *
 * @param $delta
 *   int
 *
 * @return
 *   array of block information
 */
function abbrfilter_block($op = 'list', $delta = 0) {
  if ($op == 'list') {
    $blocks[0]['info'] = t('Filtered abbr lists on submission pages');
    return $blocks;
  }
  else if ($op == 'view') {
    switch ($delta) {
      case 0:
        $block['subject'] = t('Filtered abbrs');
        $block['content'] = variable_get('display_abbrfilter_short', 0) ? _abbrfilter_table() : '';
        return $block;
    }
  }
}


/**
 * Displays a list of abbrs that will be themed as an HTML table.
 *
 * @return
 *  a drupal-themed HTML table of filtered abbreviations
 */
function _abbrfilter_table() {

  $list = _abbrfilter_list();
  foreach ($list as $filtered_abbr) {
    $rows[$filtered_abbr->id]['abbr'] = $filtered_abbr->abbrs;
    $rows[$filtered_abbr->id]['replacement'] = $filtered_abbr->replacement;
  }
  if (is_array($rows)) {
    $rows = array_reverse($rows);
  }
  $content = theme('table', NULL, $rows);
  return $content;
}

/**
 * Implementation of hook_filter_tips().
 *
 * @return
 *   a long or short notice of the filters behavior
 */
function abbrfilter_filter_tips($delta, $format, $long = FALSE) {
  if ($long) {
    return t("If you include a abbreviation in your post that's filtered, it will be augmented by an ABBR tag.") .'<br />';
    // later add a short list of ABBRs
    }
  else {
    variable_set('display_abbrfilter_short', 1);
    return t('Filtered abbreviations will be augmentedwith an ABBR tag.');
  }
}

/**
 * Implementation of hook_filter().
 *
 * @param $op
 *   the operation for the filter function to carry out
 * @param $text
 *   the text to be filtered
 *
 * @return
 *   depends on $op case.  $op = 'process' returns the filtered text
 *
 */
function abbrfilter_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('ABBR filter'));
    case 'description':
      return abbrfilter_help('admin/modules#description', array());
    case 'settings':
      $form['abbr_filter'] = array(
        '#type' => 'fieldset',
        '#title' => t('ABBR filter'),
        '#description' => t('You can define a global list of abbreviations to be filtered on the <a href="!url">ABBRfilter settings page</a>.', array('!url' => url('admin/settings/abbrfilter'))),
      );
      return $form;
    case 'process':
      return abbrfilter_filter_process($text);
    default:
      return $text;
  }
}

/**
 *  hook_filter process operation callback.
 * 
 * searches for abbreviations and replaces them with ABBR tags
 *
 * @param $text
 *   the text to be filtered
 *
 * @return
 *   returns the text, processed by the filter
 *
 */
function abbrfilter_filter_process($text) {
  $text = ' '. $text .' ';
  $list = _abbrfilter_list();
  foreach ($list as $abbr) {
    // Prevent mysterious empty value from blowing away the node title.
    if (!empty($abbr->abbrs)) {
        $text = preg_replace('/(\b'. $abbr->abbrs .'\b)(?=([^\>]*[\<]|[^\>]*$))/', '<abbr title="'. $abbr->replacement .'" >$1</abbr>', $text);
    }
  }
  $text = drupal_substr($text, 1, -1);
  return $text;
}

/**
 * Query to get the list of abbrs to filter in the
 * filter processing stage. Does not use a pager.
 *
 * @return
 *   a list of abbreviations to filter
 */
function _abbrfilter_list($refresh = 0) {
  static $list = NULL;
  if (is_null($list) || $refresh) {
    $result = db_query('SELECT * FROM {abbrfilter} ORDER BY abbrs DESC');
    $list = array();
    while ($a = db_fetch_object($result)) {
      $list[] = $a;
    }
  }
  return $list;
}

/**
 * Query to get the list of abbrs to filter. Has a
 * pager for managing long lists
 *
 * @return
 *   a list of abbreviations to filter, with a pager
 */
function _abbrfilter_admin_list($header) {
  $sql = 'SELECT * FROM {abbrfilter}';
  $result = pager_query($sql . tablesort_sql($header), 50);
  $list = array();
  while ($a = db_fetch_object($result)) {
    $list[] = $a;
  }
  return $list;
}


/**
 * abbrfilter admin settings page callback.
 *
 * @return
 *  a themed table of abbreviations to filter with links to edit and delete operations
 *
 */
function abbrfilter_admin_list() {
  $header = array(
    array('data' => t('Abbreviation'), 'field' => 'abbrs', 'sort' => 'asc'),
    array('data' => t('Unabbreviated term'), 'field' => 'replacement'),
    array('data' => t('Operations'), 'colspan' => 2)
  );
  $rows = array();
  $list = _abbrfilter_admin_list($header);
  foreach ($list as $abbr) {
    $rows[] = array(
      check_plain($abbr->abbrs),
      check_plain($abbr->replacement),

      l(t('Edit'), 'admin/settings/abbrfilter/edit/'. $abbr->id),
      l(t('Delete'), 'admin/settings/abbrfilter/delete/'. $abbr->id),
    );
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager');

  return $output;
}

/**
 * Edit abbr filter form.
 *
 * @param $abbr_id
 *   the id of the abbreviation to edit, if not a new abbreviation
 *
 * @return
 *  the form array
 */
function abbrfilter_admin_edit_form($form, $abbr_id = NULL) {
  if (!isset($abbr_id) || !is_numeric($abbr_id)) {
    drupal_set_message(t('The ABBRfilter ID of the abbr or phrase you are trying to edit is missing or is not a number.'), 'error');
    drupal_goto('admin/settings/abbrfilter');
  }
  $result = db_query('SELECT * FROM {abbrfilter} WHERE id = %d', $abbr_id);
  $abbr = db_fetch_object($result);

  $form = array();
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $abbr->id,
  );

  $form['abbrs'] = array(
    '#type' => 'textfield',
    '#title' => t('abbr or phrase to filter'),
    '#default_value' => $abbr->abbrs,
    '#description' => t('Enter the abbreviation you want to filter.'),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['replacement'] = array(
    '#type' => 'textfield',
    '#title' => t('Replacement text'),
    '#description' => t('Enter the full, unabbreviated term to which the abbreviation refers.'),
    '#default_value' => $abbr->replacement,
    '#size' => 50,
    '#maxlength' => 255,
  );
  $form['Save abbr filter'] = array(
    '#type' => 'submit',
    '#value' => t('Save abbr filter'),
  );

  return $form;
}

/**
 * Edit abbr filter form submit handler.
 *
 * writes abbreviation to table and provides a message
 */
function abbrfilter_admin_edit_form_submit($form, &$form_state) {
  $row = new stdClass;
  $row->id       = $form_state['values']['id'];
  $row->abbrs     = $form_state['values']['abbrs'];
  $row->replacement = $form_state['values']['replacement'];
  drupal_write_record('abbrfilter', $row, 'id');
  watchdog('abbrfilter', 'Updated filter for: %abbr', array('%abbr' => $row->abbrs));
  drupal_set_message(t('Updated filter for: %abbr', array('%abbr' => $row->abbrs)));
  $form_state['redirect'] = 'admin/settings/abbrfilter';
  cache_clear_all('*', 'cache_filter', TRUE);
}

/**
 * Add abbr filter form.
 *
 * form for adding multiple abbreviations at once
 *
 * @return
 *   the form array
 */
function abbrfilter_admin_add_form() {
  $form = array();

  $form['help'] = array(
    '#type' => 'markup',
    '#value' => t("Enter an abbreviation you want to augment followed by '|' and the full text unabbrevaited term. You can enter multiple abbreviation and terms pairs by hitting return and adding more. Abbreviations should be made up of numbers and letters only, are case sensitive, and must be paired with an unabbreviated term."),
  );

  $form['abbrs'] = array(
    '#type' => 'textarea',
    '#title' => t('abbrs'),
    '#description' => t("Enter a abbreviation you want to augment followed by '|' and the full term or phrase."),
    '#required' => true
  );

  $form['Save abbr filter'] = array(
    '#type' => 'submit',
    '#value' => t('Save ABBR filter')
  );

  return $form;
}

/**
 * Add abbr filter form submit handler.
 *
 * validates submission by checking for duplicate entries, invalid characters, and that there is an abbreviation and phrase pair
 *
 */
function abbrfilter_admin_add_form_submit($form, &$form_state) {
  $pairs = explode("\n", $form_state['values']['abbrs']);
  $pairs = array_map('trim', $pairs);
  $pairs = array_filter($pairs, 'strlen');
  foreach ($pairs as $pair) {
    $row = new stdClass;
    list($row->abbrs, $row->replacement) = explode('|', $pair);

    $list = _abbrfilter_list();
    $abbrduplicate='';
      foreach ($list as $abbr) {
        if ($abbr->abbrs==$row->abbrs) {
      $abbrduplicate .= 'DUPE';
      }
    }

    if ($abbrduplicate!='') {
      drupal_set_message(t('Cannot add abbreviation. %abbr already exists.', array('%abbr' => $row->abbrs)), 'error');
      $form_state['redirect'] = FALSE;  
    }
    elseif (preg_match('/^\b\w+\b$/', $row->abbrs)==FALSE) {
      drupal_set_message(t('Cannot add abbreviation. %abbr contains invalid characters.', array('%abbr' => $row->abbrs)), 'error');
      $form_state['redirect'] = FALSE;
    }
    elseif ($row->replacement) {
      drupal_write_record('abbrfilter', $row);
      watchdog('abbrfilter', 'Added filter for: %abbr', array('%abbr' => $row->abbrs));
      drupal_set_message(t('Added filter for: %abbr', array('%abbr' => $row->abbrs)));
      $form_state['redirect'] = 'admin/settings/abbrfilter';
      cache_clear_all('*', 'cache_filter', TRUE);
    }
    else {
      drupal_set_message(t('Cannot add abbreviation.  You must specify the unabbreviated text for: %abbr',  array('%abbr' => $row->abbrs)), 'error');
      $form_state['redirect'] = FALSE;
    }
  }
  
}


/**
 * Test abbr filter form.
 *
 * A form to let you test a phrase in the admin inteface
 *
 * @return
 *   the form array
 */
function abbrfilter_admin_test_filter_form($form_state) {
  $form = array();
  $form['#redirect'] = FALSE;

  $form['test_abbr'] = array(
    '#type' => 'textfield',
    '#title' => t('abbr to test'),
    '#description' => t('Enter a abbr or phrase that you want to test your abbrfilters on'),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  if ($form_state['values']['test_abbr']) {
    $form['text_result'] = array(
      '#type' => 'markup',
      '#value' => _abbrfilter_test_filter($form_state['values']['test_abbr']),
      '#prefix' => '<div class="abbrfilter-test-filter">',
      '#suffix' => '</div>',
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test abbr filters'),
  );

  return $form;
}

/**
 * Test abbr filter form submit handler.
 */
function abbrfilter_admin_test_filter_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Test abbr filter page callback.
 *
 * @param $testphrase
 *   a string to run through the filter as a test
 *
 * @return
 *   a message that shows that the phrase was filtered or not
 *
 */function _abbrfilter_test_filter($testphrase) {
  $filtered_phrase = abbrfilter_filter_process($testphrase);
  if ($testphrase == $filtered_phrase) {
    return t("Your test abbreviation '%testphrase' did not match any filters", array('%testphrase' => $testphrase));

  }
  else {
    return t("Your test abbreviation '%testphrase' was filtered to '%filtered_phrase'", array('%testphrase' => $testphrase, '%filtered_phrase' => $filtered_phrase));
  }
}
